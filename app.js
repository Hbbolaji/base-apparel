const email = document.querySelector('input')
const btn = document.querySelector('button')
const error = document.querySelector('#error')

btn.addEventListener('click', () => {
  if (email.value === "") {
    errorMessage('Email field is empty')
  } else if (!validateEmail(email.value)) {
    errorMessage('Invalid Email')
  } else {
    errorMessage('Valid Email')
  }
})

const validateEmail = (email) => {
  const emailValue = email.split('')
  return(emailValue.includes("@"))
}

const errorMessage = (msg) => {
  error.style.display = 'block'
  error.innerText = msg
  email.value = ""
  setTimeout(() => {
    error.style.display = 'none'
  }, 2000)
}